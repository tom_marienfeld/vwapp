// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import * as VueGoogleMaps from 'vue2-google-maps'
import VuejsDialog from "vuejs-dialog"
import VueTimepicker from 'vue2-timepicker'

Vue.config.productionTip = false
Vue.use(VuejsDialog)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCIc_oqs_OsuRhIyzpd_CVes1s95s2qc68',
    libraries: 'places',
  },

  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,

  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})



/* eslint-disable no-new */
new Vue({
  el: '#app',
  mounted: function() {
    $('.sidenav').sidenav();
  },
  router,
  template: '<App/>',
  components: { App }
})


