import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Music from '@/components/Music'
import Temperatures from '@/components/Temperatures'
import Settings from '@/components/Settings'
import Gasstation from '@/components/Gasstation'
import GasstationCar from '@/components/GasstationCar'
import TemperatureCar from '@/components/TemperatureCar'


Vue.use(Router)

export var routes = routes = [{
  name: 'dashboard',
  title: 'Dashboard',
  path: '/',
  icon: 'dashboard',
  inmenu: true,
  component: Dashboard
},
{
  name: 'gasstationcar',
  title: 'Tankstelle',
  inmenu: false,
  component: GasstationCar,
  path: '/gasstationcar/:id',
},
{
  name: 'music',
  title: 'Meine Musik',
  path: '/musik',
  icon: 'music_note',
  inmenu: true,
  component: Music
},
{
  name: 'Tankstellen',
  title: 'Meine Tankstellen',
  path: '/gasstation',
  icon: 'local_gas_station',
  inmenu: true,
  component: Gasstation
},
{
  name: 'temperatures',
  title: 'Meine Temperaturen',
  path: '/temperaturen',
  icon: 'whatshot',
  inmenu: true,
  component: Temperatures
},
{
  name: 'temperaturecar',
  title: 'Temperatur',
  inmenu: false,
  component: TemperatureCar,
  path: '/temperaturecar/:id',
},
{
  name: 'settings',
  title: 'Einstellungen',
  path: '/einstellungen',
  inmenu: true,
  icon: 'build',
  component: Settings
}
];

export default new Router({
  routes: routes,
  mode: 'history'
})
